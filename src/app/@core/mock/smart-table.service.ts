import { Injectable } from '@angular/core';
import { SmartTableData } from '../data/smart-table';

@Injectable()
export class SmartTableService extends SmartTableData {

  data = [
  {
    id: 1,
    tanggal: '22 Januari 2020',
    waktu: '07:43 PM',
    lokasi: 'Istana Presiden',
    tema: 'Persiapan Reshuffle Kabinet 4.0',
    jenis_rapat: 'Wajib',
    tujuan : 'Mengajak',
    pengirim : 'Sekretaris Negara'
  },
  {
    id: 2,
    tanggal: '22 Januari 2020',
    waktu: '07:43 PM',
    lokasi: 'Istana Presiden',
    tema: 'Persiapan Reshuffle Kabinet 4.0',
    jenis_rapat: 'Wajib',
    tujuan : 'Mengajak',
    pengirim : 'Sekretaris Negara'
  },
  {
    id: 3,
    tanggal: '22 Januari 2020',
    waktu: '07:43 PM',
    lokasi: 'Istana Presiden',
    tema: 'Persiapan Reshuffle Kabinet 4.0',
    jenis_rapat: 'Wajib',
    tujuan : 'Mengajak',
    pengirim : 'Sekretaris Negara'
  },
  {
    id: 4,
    tanggal: '22 Januari 2020',
    waktu: '07:43 PM',
    lokasi: 'Istana Presiden',
    tema: 'Persiapan Reshuffle Kabinet 4.0',
    jenis_rapat: 'Wajib',
    tujuan : 'Mengajak',
    pengirim : 'Sekretaris Negara'
  },
  {
    id: 5,
    tanggal: '22 Januari 2020',
    waktu: '07:43 PM',
    lokasi: 'Istana Presiden',
    tema: 'Persiapan Reshuffle Kabinet 4.0',
    jenis_rapat: 'Wajib',
    tujuan : 'Mengajak',
    pengirim : 'Sekretaris Negara'
  }
  ];

  getData() {
    return this.data;
  }
}
